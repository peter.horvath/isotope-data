DROP TABLE IF EXISTS core;
SELECT
  element,
  y,
  nuclide,
  z::integer,
  n::integer,
  REGEXP_REPLACE(mass, '(\(|#| ).*$', '')::float AS mass
INTO core
FROM step3
WHERE
  z IS NOT NULL
  AND
  n IS NOT NULL
  AND
  n ~ E'^\\d+'
  AND
  mass IS NOT NULL
  AND
  mass ~ '^[0-9]';
