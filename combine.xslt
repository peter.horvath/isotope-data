<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" version="1.0" indent="yes" encoding="utf-8"/>

  <xsl:template match="/">
    <cells>
      <xsl:for-each select="/links/link">
        <xsl:for-each select="document(concat('target/elements/',@element,'_cell.xml'))/table/cell">
          <cell>
            <xsl:copy-of select="@*"/>
          </cell>
        </xsl:for-each>
      </xsl:for-each>
    </cells>
  </xsl:template>

</xsl:stylesheet>
