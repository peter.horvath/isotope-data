<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" />

  <xsl:template match="/tbody">
    <table>
      <xsl:call-template name="generate-rows">
        <xsl:with-param name="current-row" select="1" />
        <xsl:with-param name="last-row" select="count(tr)" />
      </xsl:call-template>
    </table>
  </xsl:template>

  <xsl:template name="generate-rows">
    <xsl:param name="current-row" />
    <xsl:param name="last-row" />
    <xsl:param name="result" select="some-dummy-node-to-make-this-a-node" />
    <!-- append current-row to previous result -->
    <xsl:variable name="new-result">
      <xsl:copy-of select="$result" />
      <row num="{$current-row}">
        <xsl:choose>
          <xsl:when test="tr[$current-row]/th">
            <xsl:attribute name="head">1</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="head">0</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
        <!-- generate cells for current-row -->
        <xsl:call-template name="generate-cells">
          <xsl:with-param name="current-row" select="$current-row" />
          <xsl:with-param name="current-cell" select="1" />
          <xsl:with-param name="x" select="1" />
          <xsl:with-param name="last-cell" select="count(tr[$current-row]/th|tr[$current-row]/td)" />
          <xsl:with-param name="previous-rows" select="$result" />
        </xsl:call-template>
      </row>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$current-row &lt; $last-row">
        <!-- recursive call -->
        <xsl:call-template name="generate-rows">
          <xsl:with-param name="current-row" select="$current-row + 1" />
          <xsl:with-param name="last-row" select="$last-row" />
          <xsl:with-param name="result" select="$new-result" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <!-- return result -->
        <xsl:copy-of select="$new-result" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="generate-cells">
    <xsl:param name="current-row" />
    <xsl:param name="current-cell" />
    <xsl:param name="x" />
    <xsl:param name="last-cell" />
    <xsl:param name="previous-rows" />
    <xsl:variable name="my-cell" select="tr[$current-row]/th[$current-cell]|tr[$current-row]/td[$current-cell]" />
    <xsl:choose>
      <!-- if there's a collision, move one place to the right -->
      <xsl:when test="exsl:node-set($previous-rows)/row/cell[@x &lt;= $x and @x + @width &gt; $x and @y + @height &gt; $current-row]">
        <xsl:call-template name="generate-cells">
          <xsl:with-param name="current-row" select="$current-row" />
          <xsl:with-param name="current-cell" select="$current-cell" />
          <xsl:with-param name="x" select="$x + 1" />
          <xsl:with-param name="last-cell" select="$last-cell" />
          <xsl:with-param name="previous-rows" select="$previous-rows" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="width">
          <xsl:choose>
            <xsl:when test="$my-cell/@colspan">
              <xsl:value-of select="$my-cell/@colspan" />
            </xsl:when>
            <xsl:otherwise>1</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="height">
          <xsl:choose>
            <xsl:when test="$my-cell/@rowspan">
              <xsl:value-of select="$my-cell/@rowspan" />
            </xsl:when>
            <xsl:otherwise>1</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <cell num="{$current-cell}" y="{$current-row}" x="{$x}" width="{$width}" height="{$height}">
          <xsl:value-of select="$my-cell" />
        </cell>
        <xsl:if test="$current-cell &lt; $last-cell">
          <xsl:call-template name="generate-cells">
            <xsl:with-param name="current-row" select="$current-row" />
            <xsl:with-param name="current-cell" select="$current-cell + 1" />
            <xsl:with-param name="x" select="$x + $width" />
            <xsl:with-param name="last-cell" select="count(tr[$current-row]/th|tr[$current-row]/td)" />
            <xsl:with-param name="previous-rows" select="$previous-rows" />
          </xsl:call-template>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
