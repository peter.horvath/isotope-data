BEGIN;

-- multiplying height > 1 cells
DROP TABLE IF EXISTS step1;
WITH ints AS (
  SELECT GENERATE_SERIES(1, (SELECT MAX(height) FROM cells)) AS "int"
)
SELECT
  cells.element,
  cells.x,
  cells.y + ints.int - 1 AS y,
  cells.width,
  cells.content
INTO step1
FROM cells, ints
WHERE
  cells.head='0'
  AND
  ints.int <= cells.height;

-- matching cells by (x, width) to the table head of the same element
DROP TABLE IF EXISTS step2;
SELECT
  step1.element,
  step1.y,
  cells.content AS name,
  step1.content AS value
INTO step2
FROM step1, cells
WHERE
  step1.element = cells.element
  AND
  cells.head = '1'
  AND
  step1.x = cells.x
  AND
  step1.width = cells.width;

-- depivoting
DROP TABLE IF EXISTS step3;
SELECT
  nuclide.element,
  nuclide.y,
  nuclide.value AS "nuclide",
  z.value AS "z",
  n.value AS "n",
  mass.value AS "mass",
  halflife.value AS "halflife",
  excenergy.value AS "excenergy",
  histname.value AS "histname",
  note.value AS "note",
  decaymode.value AS "decaymode",
  abundance.value AS "abundance",
  spinparity.value AS "spinparity"
INTO step3
FROM step2 nuclide
LEFT JOIN step2 z ON nuclide.element = z.element AND nuclide.y = z.y AND z.name = 'Z'
LEFT JOIN step2 n ON nuclide.element = n.element AND nuclide.y = n.y AND n.name = 'N'
LEFT JOIN step2 mass ON nuclide.element = mass.element AND nuclide.y = mass.y AND mass.name = 'Isotopic mass (Da)'
LEFT JOIN step2 halflife ON nuclide.element = halflife.element AND nuclide.y = halflife.y AND (halflife.name = 'Half-life' OR halflife.name = 'Half-life [resonance width]')
LEFT JOIN step2 excenergy ON nuclide.element = excenergy.element AND nuclide.y = excenergy.y AND excenergy.name = 'Excitation energy'
LEFT JOIN step2 histname ON nuclide.element = histname.element AND nuclide.y = histname.y AND histname.name = 'Historic name'
LEFT JOIN step2 norm ON nuclide.element = norm.element AND nuclide.y = norm.y AND norm.name = 'Normal proportion'
LEFT JOIN step2 daughter ON nuclide.element = daughter.element AND nuclide.y = daughter.y AND daughter.name = 'Daughter isotope'
LEFT JOIN step2 vari ON nuclide.element = vari.element AND nuclide.y = vari.y AND vari.name = 'Range of variation'
LEFT JOIN step2 note ON nuclide.element = note.element AND nuclide.y = note.y AND note.name = 'Note'
LEFT JOIN step2 decaymode ON nuclide.element = decaymode.element AND nuclide.y = decaymode.y AND decaymode.name = 'Decay mode'
LEFT JOIN step2 abundance ON nuclide.element = abundance.element AND nuclide.y = abundance.y AND abundance.name = 'Isotopic abundance'
LEFT JOIN step2 spinparity ON nuclide.element = spinparity.element AND nuclide.y = spinparity.y AND spinparity.name = 'Spin and parity'
WHERE nuclide.name = 'Nuclide';

END;
