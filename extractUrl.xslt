<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" version="1.0" indent="yes" encoding="utf-8"/>

  <xsl:template name="substring-after-last">
    <xsl:param name="input" />
    <xsl:param name="marker" />
    <xsl:choose>
      <xsl:when test="contains($input,$marker)">
        <xsl:call-template name="substring-after-last">
          <xsl:with-param name="input" select="substring-after($input,$marker)" />
          <xsl:with-param name="marker" select="$marker" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$input" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/">
    <links>
      <xsl:for-each select="html//a[contains(@title, 'Isotopes of')]">
        <link>
          <xsl:attribute name="href">https://en.wikipedia.org<xsl:value-of select="@href"/></xsl:attribute>
          <xsl:attribute name="element">
            <xsl:call-template name="substring-after-last">
              <xsl:with-param name="input" select="current()"/>
              <xsl:with-param name="marker" select="' '"/>
            </xsl:call-template>
          </xsl:attribute>
        </link>
      </xsl:for-each>
    </links>
  </xsl:template>
</xsl:stylesheet>
