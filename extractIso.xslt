<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" version="1.0" indent="yes" encoding="utf-8"/>

  <xsl:template name="substring-after-last">
    <xsl:param name="input" />
    <xsl:param name="marker" />
    <xsl:choose>
      <xsl:when test="contains($input,$marker)">
        <xsl:call-template name="substring-after-last">
          <xsl:with-param name="input" select="substring-after($input,$marker)" />
          <xsl:with-param name="marker" select="$marker" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$input" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/">
    <isotopes>
      <xsl:for-each select="html//div[@id='mw-content-text']/div/table[@class='wikitable']/tbody/tr[count(td)>0]">
        <isotope>
          <xsl:attribute name="symbol"><xsl:value-of select="normalize-space(td[1]/text()[2])"/></xsl:attribute>
          <xsl:attribute name="A"><xsl:value-of select="td[1]/sup"/></xsl:attribute>
          <xsl:attribute name="Z"><xsl:value-of select="normalize-space(td[2])"/></xsl:attribute>
          <xsl:attribute name="N"><xsl:value-of select="normalize-space(td[3])"/></xsl:attribute>
          <xsl:attribute name="m"><xsl:value-of select="normalize-space(td[4])"/></xsl:attribute>
          <xsl:attribute name="excitationEnergy"><xsl:value-of select="normalize-space(td[2])"/></xsl:attribute>
          <xsl:attribute name="halfLife"><xsl:value-of select="normalize-space(td[5])"/></xsl:attribute>
          <xsl:attribute name="decayMode"><xsl:value-of select="normalize-space(td[6])"/></xsl:attribute>
          <xsl:attribute name="daughterSymbol"><xsl:value-of select="normalize-space(td[7]/text()[2])"/></xsl:attribute>
          <xsl:attribute name="daughterA"><xsl:value-of select="td[7]/sup"/></xsl:attribute>
          <xsl:attribute name="spin"><xsl:value-of select="normalize-space(td[8])"/></xsl:attribute>
          <xsl:attribute name="abundance"><xsl:value-of select="td[9]"/></xsl:attribute>
          <xsl:attribute name="abundanceVar"><xsl:value-of select="td[10]"/></xsl:attribute>
          <!--
          <xsl:attribute name="href">https://en.wikipedia.org<xsl:value-of select="@href"/></xsl:attribute>
          <xsl:attribute name="element">
            <xsl:call-template name="substring-after-last">
              <xsl:with-param name="input" select="current()"/>
              <xsl:with-param name="marker" select="' '"/>
            </xsl:call-template>
          </xsl:attribute>
          -->
          <source>
            <xsl:copy-of select="current()"/>
          </source>
        </isotope>
      </xsl:for-each>
    </isotopes>
  </xsl:template>
</xsl:stylesheet>
