<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" version="1.0" indent="yes" encoding="utf-8"/>
  <xsl:param name="ELEMENT"/>

  <xsl:template match="/">
    <table>
      <xsl:apply-templates/>
    </table>
  </xsl:template>

  <xsl:template match="cell">
    <cell>
      <xsl:attribute name="element">
        <xsl:value-of select="$ELEMENT"/>
      </xsl:attribute>
      <xsl:attribute name="head">
        <xsl:value-of select="../@head"/>
      </xsl:attribute>
      <xsl:attribute name="num"><xsl:value-of select="@num"/></xsl:attribute>
      <xsl:attribute name="x"><xsl:value-of select="@x"/></xsl:attribute>
      <xsl:attribute name="y"><xsl:value-of select="@y"/></xsl:attribute>
      <xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
      <xsl:attribute name="height"><xsl:value-of select="@height"/></xsl:attribute>
      <xsl:attribute name="content">
        <xsl:value-of select="normalize-space(text())"/>
      </xsl:attribute>
    </cell>
  </xsl:template>
  
</xsl:stylesheet>
