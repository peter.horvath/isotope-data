<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" version="1.0" indent="yes" encoding="utf-8"/>

  <xsl:template match="/">
    <xsl:for-each select="(/html//div[@id='mw-content-text']/div/table[@class='wikitable']/tbody)[1]">
      <xsl:copy>
        <xsl:apply-templates/>
      </xsl:copy>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="b|br|i|a|span">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="sup[@class='reference']|@style"/>

</xsl:stylesheet>
