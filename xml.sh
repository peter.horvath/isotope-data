#!/bin/bash -x
set -e

declare -A dontwork

dontwork=(
  ["neutronium"]=1
  ["unbinilium"]=1
  ["unbiunium"]=1
  ["ununennium"]=1
)

[ -d target/elements ] || mkdir -pv target/elements

[ -f target/list.html ] || wget -O target/list.html https://en.wikipedia.org/wiki/Category:Lists_of_isotopes_by_element

# pretty-formatting, html5 fixing into xml-compliant form
tidy -config ./tidy.conf -quiet -asxml target/list.html >target/list.xhtml || true

# removing namespace crap
xsltproc nons.xslt target/list.xhtml >target/list.xml

# extracting url data
xsltproc extractUrl.xslt target/list.xml >target/urls.xml

xsltproc args.xslt target/urls.xml|sed 's/^ *//g'|while read -r -a line
do
  element="${line[0]}"
  file="${line[1]}"
  url="${line[2]}"
  echo "element:$element file:$file url:$url." >&2

  [ "${url}" = "" ] && continue

  [ "${dontwork[$element]}" = 1 ] && continue

  [ -f "$file" ] || wget -O "$file" "$url"

  xhtmlFile="${file%.html}.xhtml"
  if ! [ -f "$xhtmlFile" ]
  then
    tidy -config tidy.conf -quiet -asxml "$file" >"$xhtmlFile" || true
  fi

  xmlRawFile="${file%.html}_raw.xml"
  if ! [ -f "$xmlRawFile" ]
  then
    xsltproc nons.xslt "$xhtmlFile" >"$xmlRawFile"
  fi

  xmlTableFile="${file%.html}_table.xml"
  if ! [ -f "$xmlTableFile" ]
  then
    xsltproc extractTable.xslt "$xmlRawFile"|tidy -config tidy.conf -quiet -asxml -xml - >"$xmlTableFile"
  fi

  xmlVTableFile="${file%.html}_vtable.xml"
  if ! [ -f "$xmlVTableFile" ]
  then
    xsltproc tableProc.xslt "$xmlTableFile"|tidy -config tidy.conf -quiet -asxml -xml - >"$xmlVTableFile"
  fi

  xmlRawCellFile="${file%.html}_rawcell.xml"
  if ! [ -f "$xmlRawCellFile" ]
  then
    xsltproc --stringparam ELEMENT "$element" makeRawCell.xslt "$xmlVTableFile" >"$xmlRawCellFile"
  fi

  xmlCellFile="${file%.html}_cell.xml"
  if ! [ -f "$xmlCellFile" ]
  then
    xsltproc normAttrs.xslt "$xmlRawCellFile" >"$xmlCellFile"
  fi

done

xsltproc combine.xslt target/urls.xml >target/cells.xml
