#!/bin/bash
psql iso -c "
DROP TABLE IF EXISTS cells;
CREATE TABLE cells (element VARCHAR(32), head CHAR(1), num INTEGER, x INTEGER, y INTEGER, width INTEGER, HEIGHT integer, CONTENT varchar(256));"
pgxmlimporter -c 'dbname=iso' <target/cells.xml
