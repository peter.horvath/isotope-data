<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text" version="1.0" indent="yes" encoding="utf-8"/>

  <xsl:template match="/">
    <xsl:for-each select="links/link">
      <xsl:value-of select="@element"/> target/elements/<xsl:value-of select="@element"/>.html <xsl:value-of select="@href"/>
      <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
